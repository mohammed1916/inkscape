#ifndef ABORTBOOLDIALOG_H
#define ABORTBOOLDIALOG_H

#include <gtkmm.h>
#include <gtkmm/builder.h>

namespace Inkscape {
namespace UI {
namespace Dialog {

class AbortBoolDialog
{
public:
    static void abort();

private:
};

} // namespace Dialog
} // namespace UI
} // namespace Inkscape

#endif // ABORTBOOLDIALOG_H

/*
  Local Variables:
  mode:c++
  c-file-style:"stroustrup"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:fileencoding=utf-8:textwidth=99 :

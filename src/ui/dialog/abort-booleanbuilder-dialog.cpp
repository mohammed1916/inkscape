#include "abort-booleanbuilder-dialog.h"

#include "io/resource.h"
#include "ui/view/svg-view-widget.h"

using namespace Inkscape::IO;
using namespace Inkscape::UI::View;

namespace Inkscape {
namespace UI {
namespace Dialog {

static Gtk::Window *window = nullptr;

void AbortBoolDialog::abort()
{
    if (!window) {
        std::string gladefile = get_filename_string(Inkscape::IO::Resource::UIS, "dialog-bool-builder.glade");

        Glib::RefPtr<Gtk::Builder> builder;
        try {
            builder = Gtk::Builder::create_from_file(gladefile);
        } catch (const Glib::Error &ex) {
            g_error("Glade file loading failed for bool error dialog");
            return;
        }
        builder->get_widget("dialog-bool-builder", window);
        if (!window) {
            g_error("file is missing or does not have the right ids.");
            return;
        }
    }
    if (window) {
        window->show();
    } else {
        g_error("Error screen window couldn't be loaded. Missing window id in glade file.");
    }
}

} // namespace Dialog
} // namespace UI
} // namespace Inkscape

/*
  Local Variables:
  mode:c++
  c-file-style:"stroustrup"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:fileencoding=utf-8:textwidth=99 :